# TRIAGE Arg

__Transforms raw command line arguments into a vector of tuples.__

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/op8d/triage_arg/>  
▶&nbsp; __Homepage:__ <https://op8d.gitlab.io/triage_arg/>  

Build and open the documentation:  
`cargo doc --no-deps --open`

## Examples

### Example 01: Basic ArgsCooked::try_new() usage
   `cargo run --example 01`
