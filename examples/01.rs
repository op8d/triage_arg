//! examples/01.rs
//! Example 01: Basic ArgsCooked::try_new() usage

use triage_arg::ArgsCooked;

fn main() {
    println!("\n> foo.bar=baz abc.xyz=123");
    let result = format!("{:?}", ArgsCooked::try_new("foo.bar=baz abc.xyz=123"));
    assert_eq!(result, "Ok(ArgsCooked { options: [(\"foo\", \"bar\", \"baz\"), (\"abc\", \"xyz\", \"123\")] })");
    println!("{}", result);
}
