//! src/args_cooked.rs

use std::fmt;

#[derive(Debug)]
pub struct ArgsCooked {
    options: Vec<(String, String, String)>,
}

impl ArgsCooked {

    pub fn try_new<S: Into<String>>(
        args_raw: S,
    ) -> Result<Self, String> {
        let args = format!("{} ", args_raw.into());
        let mut options: Vec<(String, String, String)> = vec![];
        let mut option: (String, String, String) = (
            String::new(), String::new(), String::new()
        );
        let mut part = Part::Group;
        for c in args.chars() {
            match c {
                '.' => part = Part::Key,
                '=' => part = Part::Value,
                ' ' => {
                    part = Part::Group;
                    options.push(option);
                    option = (String::new(), String::new(), String::new());
                },
                _ => {
                    match part {
                        Part::Group => option.0.push(c),
                        Part::Key => option.1.push(c),
                        Part::Value => option.2.push(c),
                    }
                }
            }
        }
        Ok(Self {
            options,
        })
    }
}

impl fmt::Display for ArgsCooked {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        for option in &self.options {
            fmt.write_str(&format!("{:?}", option)[0..])?;
        }
        Ok(())
    }
}

enum Part {
    Group,
    Key,
    Value,
}
